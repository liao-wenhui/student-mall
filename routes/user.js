//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const r=express.Router();
//往路由器中添加路由
//1.用户注册接口(post /reg)
//接口的地址：http://127.0.0.1:8080/v1/users/reg
//请求的方法：post
r.post('/reg',(req,res,next)=>{
  //1.1获取post请求的数据
  let obj=req.body;
  console.log(obj);
  //1.2验证各项数据是否为空
  if(!obj.uname){
    res.send({code:401,msg:'uname不能为空'});
    //阻止往后执行
	return;
  }
  if(!obj.upwd){
    res.send({code:402,msg:'upwd不能为空'});
	return;
  }
  if(!obj.email){
    res.send({code:403,msg:'email不能为空'});
	return;
  }
  if(!obj.phone){
    res.send({code:404,msg:'phone不能为空'});
	return;
  }
  //验证手机号码格式
  //如果不符合规则
  if(!/^1[3-9]\d{9}$/.test(obj.phone)){
    res.send({code:405,msg:'手机号码格式错误'});
	return;
  }
  //1.3执行SQL命令
  pool.query('insert into xz_user set ?',[obj],(err,result)=>{
    if(err){
	  //如果出现错误交给下一个中间件来处理
      next(err);
	  //阻止往后执行
      return;
	}
	console.log(result);
    res.send({code:200,msg:'注册成功'});
  });
});
//2.用户登录的接口(post /login)
//接口的地址：http://127.0.0.1:8080/v1/users/login
//请求的方法：post
r.post('/login',(req,res,next)=>{
  //2.1获取post请求的数据
  let obj=req.body;
  console.log(obj);
  //2.2验证数据是否为空
  if(!obj.uname){
    res.send({code:401,msg:'uname不能为空'});
	return;
  }
  if(!obj.upwd){
    res.send({code:402,msg:'upwd不能为空'});
	return;
  }
  //2.3执行SQL命令，到数据库中查询有没有用户名和密码匹配的数据
  pool.query('select * from xz_user where uname=? and upwd=?',[obj.uname,obj.upwd],(err,result)=>{
    if(err){
	  //有错误把它交个下一个错误处理中间件
	  next(err);
	  return;
	}
	console.log(result);
	//结果是数组，如果是空数组，说明登录失败，否则登录成功
	if(result.length===0){
	  res.send({code:201,msg:'登录失败'});
	}else{
	  res.send({code:200,msg:'登录成功'});
	}
  });
});
//3.修改用户资料接口(put  /)
//接口的地址：http://127.0.0.1:8080/v1/users
//请求的方法：put
r.put('/',(req,res,next)=>{
  //3.1获取post传递的数据
  let obj=req.body;
  console.log(obj);
  //3.2验证各项数据是否为空
  //遍历属性
  //初始化变量用于保存状态码
  let i=400;
  for(let k in obj){
	//每遍历一个属性，状态码加1
	i++;
	//k属性名   obj[k]属性值
    //console.log(k,obj[k]);
	//如果属性值为空，提示属性名这项不能为空
	if(!obj[k]){
	  res.send({code:i,msg:k+'不能为空'});
	  return;
	}
  }
  //验证手机号码
  if(!/^1[3-9]\d{9}$/.test(obj.phone)){
    res.send({code:406,msg:'手机号码格式错误'});
	return;
  }
  //验证邮箱格式
  if(!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(obj.email)){
    res.send({code:407,msg:'邮箱格式错误'});
	return;
  }


  //3.3执行SQL命令
  pool.query('update xz_user set ? where uid=?',[obj,obj.uid],(err,result)=>{
    if(err){
	  next(err);
	  return;
	}
    console.log(result);
	//结果是对象，如果对象下的affectedRows值为0说明修改失败，否则修改成功
	if(result.affectedRows===0){
	  res.send({code:201,msg:'修改失败'});
	}else{
	  res.send({code:200,msg:'修改成功'});
	}
  });
});
//4.检索用户接口(get /detail/编号)
//接口的地址：http://127.0.0.1:8080/v1/users/detail/2
//请求的方法：get
r.get('/detail/:uid',(req,res,next)=>{
  //4.1获取路由传参的数据
  let obj=req.params;
  console.log(obj);
  //4.2执行SQL命令，查询用户
  pool.query('select * from xz_user where uid=?',[obj.uid],(err,result)=>{
    if(err){
	  next(err);
	  return;
	}
    console.log(result);
	//结果是数组，如果为空数组表示用户不存在，否则查询成功
	if(result.length===0){
	  res.send({code:201,msg:'该用户不存在'});
	}else{
	  res.send({code:200,msg:'检索成功',data:result});
	}
  });  
});
//检测用户是否被占用
r.get('/checkUname/:uname',(req,res,next)=>{});
//检测邮箱是否被占用
r.get('/checkEmail/:email',(req,res,next)=>{});
//检测手机是否被占用
r.get('/checkPhone/:phone',(req,res,next)=>{});

//导出路由器对象
module.exports=r;