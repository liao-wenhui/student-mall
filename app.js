//引入express模块
const express=require('express');
//创建web服务器
const app=express();
//引入用户路由器模块
const userRouter=require('./routes/user.js')
//console.log(userRouter);
//设置端口
app.listen(8080);
//使用body-parser中间件，可以将post请求数据解析为对象
app.use(express.urlencoded({
  extended:false //内部任何解决为对象
}));
//使用路由器，将路由器下的路由挂载到了web服务器下
//同时使用路由url添加前缀/v1/users
app.use('/v1/users',userRouter)
//添加错误处理中间件，拦截所有路由中产生的错误
app.use((err,req,res,next)=>{
//接收到的路由传递过来的错误信息
console.log(err);
res.status(500).send({code:500,msg:'服务器端错误'})
});



